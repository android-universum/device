@Device-Screen
===============

This module groups the following modules into one **single group**:

- [Screen-Core](https://bitbucket.org/android-universum/device/src/main/library-screen-core)
- [Screen-Util](https://bitbucket.org/android-universum/device/src/main/library-screen-util)

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adevice/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adevice/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:device-screen:${DESIRED_VERSION}@aar"

_depends on:_
[device-core](https://bitbucket.org/android-universum/device/src/main/library-core)