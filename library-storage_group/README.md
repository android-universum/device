@Device-Storage
===============

This module groups the following modules into one **single group**:

- [Storage-Core](https://bitbucket.org/android-universum/device/src/main/library-storage-core)
- [Storage-Util](https://bitbucket.org/android-universum/device/src/main/library-storage-util)

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adevice/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adevice/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:device-storage:${DESIRED_VERSION}@aar"

_depends on:_
[device-core](https://bitbucket.org/android-universum/device/src/main/library-core)