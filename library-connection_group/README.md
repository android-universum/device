@Device-Connection
===============

This module groups the following modules into one **single group**:

- [Connection-Core](https://bitbucket.org/android-universum/device/src/main/library-connection-core)
- [Connection-Util](https://bitbucket.org/android-universum/device/src/main/library-connection-util)

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adevice/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adevice/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:device-connection:${DESIRED_VERSION}@aar"
