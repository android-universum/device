Modules
===============

Library is also distributed via **separate modules** which may be downloaded as standalone parts of
the library in order to decrease dependencies count in Android projects, so only dependencies really
needed in an Android project are included. **However** some modules may depend on another modules
from this library or on modules from other libraries.

## Download ##

### Gradle ###

For **successful resolving** of artifacts for separate modules via **Gradle** add the following snippet
into **build.gradle** script of your desired Android project and use `implementation '...'` declaration
as usually.

    repositories {
        maven {
            url  "http://dl.bintray.com/universum-studios/android"
        }
    }


## Available modules ##
> Following modules are available in the [latest](https://bitbucket.org/android-universum/device/downloads "Downloads page") stable release.

- **[Core](https://bitbucket.org/android-universum/device/src/main/library-core)**
- **[Battery](https://bitbucket.org/android-universum/device/src/main/library-battery)**
- **[@Connection](https://bitbucket.org/android-universum/device/src/main/library-connection_group)**
- **[Connection-Core](https://bitbucket.org/android-universum/device/src/main/library-connection-core)**
- **[Connection-Util](https://bitbucket.org/android-universum/device/src/main/library-connection-util)**
- **[@Screen](https://bitbucket.org/android-universum/device/src/main/library-screen_group)**
- **[Screen-Core](https://bitbucket.org/android-universum/device/src/main/library-screen-core)**
- **[Screen-Util](https://bitbucket.org/android-universum/device/src/main/library-screen-util)**
- **[@Storage](https://bitbucket.org/android-universum/device/src/main/library-storage_group)**
- **[Storage-Core](https://bitbucket.org/android-universum/device/src/main/library-storage-core)**
- **[Storage-Util](https://bitbucket.org/android-universum/device/src/main/library-storage-util)**
- **[Util](https://bitbucket.org/android-universum/device/src/main/library-util)**
