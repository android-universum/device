Change-Log
===============
> Regular configuration update: _09.03.2020_

## Version 1.x ##

### 1.1.1 ###
> 07.02.2019

- Re-deploy for version **1.1.0** due to missing **aar** artifacts.

### 1.1.0 ###
> 11.01.2019

- Regular **dependencies update** (mainly to use new artifacts from **Android Jetpack**).

### 1.0.3 ###
> 04.06.2018
 
- Small updates and improvements.

### 1.0.2 ###
> 03.12.2017 

- Small updates and improvements.

### 1.0.1 ###
> 26.03.2017

- Added missing `Battery` module with sources into **primary** library artifact.

### 1.0.0 ###
> 25.12.2016

- First production release.