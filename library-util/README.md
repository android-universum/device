Device-Util
===============

This module contains utilities that may be used to obtain additional information about **Android** device.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Adevice/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Adevice/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:device-util:${DESIRED_VERSION}@aar"

_depends on:_
[device-screen-core](https://bitbucket.org/android-universum/device/src/main/library-screen-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [DeviceUtils](https://bitbucket.org/android-universum/device/src/main/library-util/src/main/java/universum/studios/android/device/DeviceUtils.java)